package games.indigo.playerbanks;

import games.indigo.playerbanks.utils.ItemBuilder;
import games.indigo.playerbanks.utils.Text;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;

public class BankGUI {

    private Main instance = Main.getInstance();

    public void open(Player player, final int page) {
        player.closeInventory();

        gui(player, player, page);
    }

    public void openOthers(Player player, OfflinePlayer owner, int page) {
        player.closeInventory();

        gui(player, owner, page);
    }

    private void gui(Player player, OfflinePlayer owner, int page) {
        Bukkit.getScheduler().runTaskAsynchronously(instance, () -> {
            Inventory inv = Bukkit.createInventory(null, 54, Text.colour("&8" + owner.getName() + "'s Bank (Page " + page + ")"));

            inv.setContents(instance.getBankManager().getPage(owner.getUniqueId().toString(), page).getContents());

            Bukkit.getScheduler().runTask(instance, () -> {
                Bukkit.getScheduler().runTaskLater(instance, () -> instance.getBankManager().setViewingBank(player, page), 2);

                for (int i = 0; i < 9; i++) inv.setItem(i, filler()); // Populate header with fillers

                if (page > 1) inv.setItem(0, previousPage(page));
                inv.setItem(3, deposit());
                inv.setItem(4, search());
                inv.setItem(5, smallDeposit());
                inv.setItem(8, nextPage(player, page));

                player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);
                player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);

                player.openInventory(inv);
            });
        });
    }

    private ItemStack filler() {
        return ItemBuilder.buildItem(Material.GRAY_STAINED_GLASS_PANE, 1, " ");
    }

    private ItemStack deposit() {
        return ItemBuilder.buildItem(Material.CHEST, 1, "&c&lDeposit all items", Arrays.asList("&7Click to deposit all the items", "&7in your inventory!"));
    }

    private ItemStack search() {
        return ItemBuilder.buildItem(Material.COMPASS, 1, "&a&lSearch for items", Arrays.asList("&7Click to search for items", "&7across all your bank pages!"));
    }

    private ItemStack smallDeposit() {
        return ItemBuilder.buildItem(Material.HOPPER, 1, "&6&lDeposit some items", Arrays.asList("&7Click to deposit all your items", "&7excluding those on your hotbar!"));
    }

    private ItemStack previousPage(int page) {
        ItemStack item = ItemBuilder.buildItem(Material.GLASS_PANE, 1, "&a&l< " + "&f&lPage " + (page - 1));
        ItemMeta meta = item.getItemMeta();
        meta.setCustomModelData(1);
        item.setItemMeta(meta);
        return item;
    }

    private ItemStack nextPage(Player player, int page) {
        int newPage = page + 1;
        ItemStack item;
        if (player.hasPermission("indigo.playerbanks." + newPage)) {
            item = ItemBuilder.buildItem(Material.GLASS_PANE, 1, "&f&lPage " + newPage + " &a&l>");
            ItemMeta meta = item.getItemMeta();
            meta.setCustomModelData(3);
            item.setItemMeta(meta);
        } else {
            item = ItemBuilder.buildItem(Material.BARRIER, 1, "&f&lPage " + newPage + " &c&l>", Arrays.asList("&7Page &c&lLOCKED", "", "&7Purchase this page to", "&7unlock it!", "", "&d&lPRICE", " - &b&l100 &3&lCrystals", "", "&6&l(!) &eClick to purchase!"));
            ItemMeta meta = item.getItemMeta();
            meta.setCustomModelData(3);
            item.setItemMeta(meta);
        }

        return item;
    }

}
