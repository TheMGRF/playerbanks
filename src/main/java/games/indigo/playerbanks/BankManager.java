package games.indigo.playerbanks;

import games.indigo.playerbanks.utils.ItemSerializer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.metadata.FixedMetadataValue;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class BankManager {

    private Main main = Main.getInstance();

    private final String DATABASE_NAME = "indigo";

    /**
     * Create the database table if it does not exist
     */
    public void createDatabase() {
        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {
            try {
                Connection con = main.getDatabase().getConnection(DATABASE_NAME);

                PreparedStatement ps = con.prepareStatement(
                        "CREATE TABLE IF NOT EXISTS `player_banks` (" +
                                "`owner` VARCHAR(36) NULL DEFAULT NULL, " +
                                "`page` TINYINT(3) UNSIGNED NULL DEFAULT NULL, " +
                                "`inventory` BLOB NULL, " +
                                "INDEX `owner` (`owner`)" +
                                ") COLLATE='latin1_swedish_ci' ENGINE=InnoDB;"
                );

                ps.execute();
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void save(String uuid, int page, String base64) {
        Bukkit.getScheduler().runTaskAsynchronously(main, () -> {
            try {
                Connection con = main.getDatabase().getConnection(DATABASE_NAME);

                PreparedStatement ps = con.prepareStatement("INSERT INTO player_banks (uuid, inventory) VALUES ('" + uuid + "|" + page + "', '" + base64 + "') ON DUPLICATE KEY UPDATE inventory = '"  + base64 + "';");

                ps.execute();
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public Inventory getPage(String uuid, int page) {
        String base64 = "";
        try {
            Connection con = main.getDatabase().getConnection(DATABASE_NAME);

            PreparedStatement ps = con.prepareStatement("SELECT inventory FROM player_banks WHERE uuid = '" + uuid + "|" + page + "';");
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                base64 = rs.getString("inventory");
            }

            ps.execute();
            ps.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return base64.equals("") ? Bukkit.createInventory(null, 54) : ItemSerializer.fromBase64(base64);
    }

    public void setViewingBank(Player player, int page) {
        switch (page) {
            case -1:
                player.removeMetadata("viewingBank", main);
                break;
            case 0:
                player.setMetadata("viewingBank", new FixedMetadataValue(main, 1));
                break;
            default:
                player.setMetadata("viewingBank", new FixedMetadataValue(main, page));
                break;
        }
    }

    public boolean isViewingBank(Player player) {
        return player.hasMetadata("viewingBank");
    }

    public int getCurrentPage(Player player) {
        return player.getMetadata("viewingBank").get(0).asInt();
    }
}
