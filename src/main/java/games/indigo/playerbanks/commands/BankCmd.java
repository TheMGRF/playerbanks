package games.indigo.playerbanks.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import games.indigo.playerbanks.BankGUI;
import org.bukkit.entity.Player;

@CommandAlias("bank|banks")
@CommandPermission("indigo.playerbanks.open")
public class BankCmd extends BaseCommand {

    @Default
    public void exploration(Player player, String args[]) {
        new BankGUI().open(player, 1);
    }

}
