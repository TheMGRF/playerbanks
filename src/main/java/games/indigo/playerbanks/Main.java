package games.indigo.playerbanks;

import co.aikar.commands.PaperCommandManager;
import games.indigo.databaseconnector.DatabaseConnector;
import games.indigo.playerbanks.commands.BankCmd;
import games.indigo.playerbanks.listeners.InventoryClickListener;
import games.indigo.playerbanks.listeners.InventoryCloseListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    private static Main instance;

    private static DatabaseConnector database;
    private static BankManager bankManager;

    public void onEnable() {
        instance = this;
        database = Bukkit.getServicesManager().getRegistration(DatabaseConnector.class).getProvider();
        bankManager = new BankManager();

        // Listeners
        Bukkit.getPluginManager().registerEvents(new InventoryCloseListener(), this);
        Bukkit.getPluginManager().registerEvents(new InventoryClickListener(), this);

        // Commands
        PaperCommandManager manager = new PaperCommandManager(this);
        manager.registerCommand(new BankCmd());

        getBankManager().createDatabase();
    }

    public static Main getInstance() {
        return instance;
    }

    DatabaseConnector getDatabase() {
        return database;
    }

    public BankManager getBankManager() {
        return bankManager;
    }
}
