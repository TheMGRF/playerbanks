package games.indigo.playerbanks.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

public class ItemBuilder {

    /**
     * Build an item stack
     * @param material The material to use in the item stack
     * @param amount How many items should be in the item stack
     * @param name The display name of the item stack
     * @return The finished item stack
     */
    public static ItemStack buildItem(Material material, int amount, String name) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(Text.colour(name));

        item.setItemMeta(itemMeta);

        return item;
    }

    /**
     * Build an item stack
     * @param material The material to use in the item stack
     * @param amount How many items should be in the item stack
     * @param name The display name of the item stack
     * @param description The lore of the item stack
     * @return The finished item stack
     */
    public static ItemStack buildItem(Material material, int amount, String name, List<String> description) {
        ItemStack item = new ItemStack(material, amount);
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setDisplayName(Text.colour(name));
        itemMeta.setLore(Text.colourArray(description));

        item.setItemMeta(itemMeta);

        return item;
    }

}
