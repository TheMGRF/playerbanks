package games.indigo.playerbanks.listeners;

import games.indigo.playerbanks.Main;
import games.indigo.playerbanks.utils.ItemSerializer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;

public class InventoryCloseListener implements Listener {

    private Main main = Main.getInstance();

    @EventHandler
    public void onInvClose(InventoryCloseEvent e) {
        Player player = (Player) e.getPlayer();

        //e.getPlayer().sendMessage("REASON: " + e.getReason().name());

        if (main.getBankManager().isViewingBank(player)) {
            String base64 = ItemSerializer.toBase64(e.getInventory());
            if (!base64.equals("rO0ABXcEAAAANnBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcA==\n")) main.getBankManager().save(player.getUniqueId().toString(), main.getBankManager().getCurrentPage(player), base64);
            main.getBankManager().setViewingBank(player, -1);
        }
    }

}
