package games.indigo.playerbanks.listeners;

import games.indigo.playerbanks.BankGUI;
import games.indigo.playerbanks.Main;
import games.indigo.playerbanks.utils.ItemSerializer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryClickListener implements Listener {

    private Main main = Main.getInstance();

    @EventHandler
    public void onInvClick(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        if (main.getBankManager().isViewingBank(player)) {
            if (e.getClickedInventory() != null && e.getClickedInventory() == e.getView().getTopInventory() && e.getSlot() < 9 && e.getCurrentItem() != null) {
                e.setCancelled(true);

                int page = main.getBankManager().getCurrentPage(player);
                BankGUI bankGUI = new BankGUI();
                switch (e.getSlot()) {
                    case 0: // previous page
                        main.getBankManager().save(player.getUniqueId().toString(), page, ItemSerializer.toBase64(e.getInventory()));
                        bankGUI.open(player, page - 1);
                        break;
                    case 3: // deposit all items
                        break;
                    case 4: // search for items
                        break;
                    case 5: // deposit all but hotbar
                        break;
                    case 8: // next page
                        main.getBankManager().save(player.getUniqueId().toString(), page, ItemSerializer.toBase64(e.getInventory()));
                        bankGUI.open(player, page + 1);
                        break;
                    default:
                        break;
                }
            }
        }
    }

}
